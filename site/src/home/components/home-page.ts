import { Component, OnInit } from '@angular/core';
import { Inject, HostListener, ViewChild } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { KSSwiperContainer, KSSwiperSlide } from 'angular2-swiper';

@Component({
	template: require('./home-template.html')
})

export class HomePage implements OnInit {
	classAnimate: boolean;

	@ViewChild(KSSwiperContainer) swiperContainer: KSSwiperContainer;
	
	example1SwipeOptions: any;

	// animate scroll
	public solutionsAnimate: boolean = false;
	public aboutAnimate: 	 boolean = false;
	public blogPostsAnimate: boolean = false;

	constructor(@Inject(DOCUMENT) private document: Document) {
		this.example1SwipeOptions = {
			pagination: '.swiper-pagination',
			paginationClickable: true,
			slidesPerView: 6,
			spaceBetween: 30,
			breakpoints: {
	          	768: {
	              slidesPerView: 5,
	              spaceBetween: 30
	          	},
	          	640: {
	              slidesPerView: 1,
	              spaceBetween: 10
	          	}
      		}
		};
	}

	moveNext() {
		this.swiperContainer.swiper.slideNext();
	}

	movePrev() {
		this.swiperContainer.swiper.slidePrev();
	}

	@HostListener("window:scroll", [])

	onWindowScroll() {
		let number = this.document.body.scrollTop;
		
		// solutions
		if (number > 780) {
		  this.solutionsAnimate = true;
		} else {
		  this.solutionsAnimate = false;
		}

		// Whath
		if (number > 1120) {
		  this.aboutAnimate = true;
		}

		// Latest posts
		if (number > 2000) {
		  this.blogPostsAnimate = true;
		}

	}

	time() {
		setTimeout( () => {
			this.classAnimate = true;
		}, 1000)
	}

	scrollAnimation(){

		let body = document.body;
		let elmt = document.documentElement;

		if (elmt.scrollTop < 200) {
			console.log('test');
		}
	}

	ngOnInit() {
		this.time();
		this.scrollAnimation();
	}
}
