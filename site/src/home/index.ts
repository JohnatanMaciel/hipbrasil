import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SharedModule } from 'src/shared';
import { HomePage } from './components/home-page';

// => Plugins
import { KSSwiperContainer, KSSwiperSlide } from 'angular2-swiper';

const routes: Routes = [
  {path: '', component: HomePage}
];

@NgModule({
  declarations: [
    HomePage,
    KSSwiperContainer,
    KSSwiperSlide
  ],
  imports: [
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class HomeModule {}
