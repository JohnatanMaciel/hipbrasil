import { Component } from '@angular/core';

@Component({
  selector: 'app',
  template: `
	<div class="menu-content">
	  <div class="menu-wrapper">

	    <h1>
	      <a routerLink="/">
	        <img src="http://empreendedoramente.com/assets/images/logo-hipbrasil.png">
	      </a>
	    </h1>

	    <nav>
	      <a routerLink="/" class="active"><p>Soluções</p></a>
	      <a routerLink="/projects"><p>Clientes</p></a>
	      <a routerLink="/projects"><p>Blog</p></a>
	      <a routerLink="/projects"><p>Contato</p></a>
	      <a routerLink="/projects"><p>Orçamento online</p></a>
	    </nav>

	    <ul class="social-media-hip">
	      <li><a href="#"><span class="fa fa-facebook"></span></a></li>
	      <li><a href="#"><span class="fa fa-linkedin"></span></a></li>
	      <li><a href="#"><span class="fa fa-google"></span></a></li>
	    </ul>

	  </div>
	</div>

	<main>
		<router-outlet></router-outlet>
	</main>

	<footer class="footer">
		<div class="locais-wrapper">
			<a href="#" class="local-item" style="background-image: url('assets/images/sao--paulo.jpg');">
				<h2>São Paulo</h2>
				<p>+55 11 <b>3230-9744</b></p>
			</a>
			<a href="#" class="local-item" style="background-image: url('assets/images/Fortaleza-289-anos.jpg');">
				<h2>Fortaleza</h2>
				<p>+55 85 <b>4042-2700</b></p>
			</a>
		</div>
		<div class="brand-footer_hip">
			<img src="http://empreendedoramente.com/assets/images/hip-logo.png"/>
			<p>©2016 HIP Brasil. All Rights Reserved.</p>
		</div>
		<div class="end-footer">
			<p><b>Entre em contato:</b> +55 (11) 3230-9744 / (85) 4042-2700</p>
			<ul>
				<li><a href="#">Vem com a gente</a></li>
				<li><a href="#">Termos de uso</a></li>
				<li><a href="#">Privacidade</a></li>
			</ul>
		</div>
	</footer>
  `
})

export class AppComponent {}
